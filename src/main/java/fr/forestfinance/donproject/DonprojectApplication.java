package fr.forestfinance.donproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = "fr.forestfinance.donproject.entity")
@EnableJpaRepositories(basePackages = "fr.forestfinance.donproject.repository")
@ComponentScan({"fr.forestfinance.donproject.service", "fr.forestfinance.donproject.restController", "fr.forestfinance.donproject"})
public class DonprojectApplication {

	public static void main(String[] args) {

	    SpringApplication.run(DonprojectApplication.class, args);
	}


}
