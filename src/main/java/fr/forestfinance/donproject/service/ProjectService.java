package fr.forestfinance.donproject.service;


import fr.forestfinance.donproject.entity.Project;

import java.util.Collection;

/**
 * Created by SebUndefined on 23/10/17.
 */
public interface ProjectService {


    Collection<Project> findAll();
}
