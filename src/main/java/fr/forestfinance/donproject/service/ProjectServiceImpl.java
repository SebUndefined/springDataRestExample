package fr.forestfinance.donproject.service;

import fr.forestfinance.donproject.entity.Project;
import fr.forestfinance.donproject.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

/**
 * Created by SebUndefined on 23/10/17.
 */
@Service
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Collection<Project> findAll() {
        return projectRepository.findAll();
    }
}
