package fr.forestfinance.donproject.restController;

import fr.forestfinance.donproject.entity.Project;
import fr.forestfinance.donproject.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;


/**
 * Created by SebUndefined on 23/10/17.
 */
@RestController
public class ProjectController {

    private final ProjectService projectService;

    @Autowired
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    @GetMapping("/all")
    public Collection<Project> indexProject() {


        return projectService.findAll();

    }
}
