package fr.forestfinance.donproject;

import fr.forestfinance.donproject.entity.Category;
import fr.forestfinance.donproject.entity.Project;
import fr.forestfinance.donproject.repository.CategoryRepository;
import fr.forestfinance.donproject.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.stream.Stream;

/**
 * Created by SebUndefined on 23/10/17.
 */
@Component
public class ProjectCommandLineRunner implements CommandLineRunner {

    private final ProjectRepository projectRepository;
    private final CategoryRepository categoryRepository;

    @Autowired
    public ProjectCommandLineRunner(ProjectRepository projectRepository, CategoryRepository categoryRepository) {
        this.projectRepository = projectRepository;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public void run(String... strings) throws Exception {
        /*Stream.of("Kentucky Brunch Brand Stout", "Good Morning", "Very Hazy", "King Julius",
                "Budweiser", "Coors Light", "PBR").forEach(name ->
                projectRepository.save(new Project(name))
        );*/
        Category category = new Category("CategorieProject");
        categoryRepository.save(category);
        for (int i = 0; i<5; i++) {
            Project project = new Project("Project " + i, category);
            projectRepository.save(project);
        }
        projectRepository.findAll().forEach(System.out::println);
    }
}
