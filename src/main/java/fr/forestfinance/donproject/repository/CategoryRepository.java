package fr.forestfinance.donproject.repository;

import fr.forestfinance.donproject.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by SebUndefined on 23/10/17.
 */
@RepositoryRestResource
public interface CategoryRepository extends JpaRepository<Category, Long> {
}
